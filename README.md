# VCI Crud

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0. This application is primarily an in-memory Angular application that aims to mock the responses of database transactions. The classes resemble a database models. If you need to understand the database models, feel free to message me.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Inspiration

Thank you to this [article](https://oraclefrontovik.com/2020/01/19/angular-crud-1-in-memory-web-api-installation-and-configuration/) for guiding me in creating this demo app.

export class SKU {
	constructor(public id = 0, public name = '', public price = 0.0) { }
}

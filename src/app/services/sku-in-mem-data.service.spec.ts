import { TestBed } from '@angular/core/testing';

import { SkuInMemDataService } from './sku-in-mem-data.service';

describe('SkuInMemDataService', () => {
  let service: SkuInMemDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SkuInMemDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

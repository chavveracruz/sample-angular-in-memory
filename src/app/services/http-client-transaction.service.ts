import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
 
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Transaction } from '../classes/transaction';
import { TransactionService } from './transaction.service';
 
const cudOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'})};
 
@Injectable()
 
export class HttpClientTransactionService extends TransactionService {
   
  constructor(private http: HttpClient) {
    super();
   }
 
  getTransactions(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(this.transactionsUrl).pipe(
      catchError(this.handleError)
    );
  }
 
  // get by id - will 404 when id not found
  getTransaction(id: number): Observable<Transaction> {
    const url = `${this.transactionsUrl}/${id}`;
    return this.http.get<Transaction>(url).pipe(
      catchError(this.handleError)
    );
  }
 
  addTransaction(sku_id: number, order_id: number): Observable<Transaction> {
    const Transaction = { sku_id, order_id };
 
    return this.http.post<Transaction>(this.transactionsUrl, transaction, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  deleteTransaction(transaction: number | Transaction): Observable<Transaction> {
    const id = typeof Transaction === 'number' ? transaction : transaction.id;
    const url = `${this.transactionsUrl}/${id}`;
 
    return this.http.delete<Transaction>(url, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  updateTransaction(transaction: Transaction): Observable<Transaction> {
    return this.http.put<Transaction>(this.transactionsUrl, transaction, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
   
  private handleError(error: any) {
    console.error(error);
    return throwError(error);    
  }
 
}
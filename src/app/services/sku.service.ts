import { SKU } from '../classes/sku';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SKUService {

	abstract getSKUs(): Observable<SKU[]>;
	abstract getSKU(id: number): Observable<SKU>;
	abstract addSKU(name: string, price: number): Observable<SKU>;
	abstract deleteSKU(SKU: SKU | number): Observable<SKU>;
	abstract searchSKU(term: string): Observable<SKU[]>;
	abstract updateSKU(SKU: SKU): Observable<SKU>;
}

import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Order } from '../classes/order';


@Injectable({
  providedIn: 'root'
})
export class OrderInMemDataService implements InMemoryDbService {

  	createDb() {
	    let orders: Order[] = [
	      { id: 1, date: '6/5/2020, 1:03 AM', user_id: 1},
	      { id: 2, date: '6/5/2020, 2:03 AM', user_id: 2},
	      { id: 3, date: '6/5/2020, 3:03 AM', user_id: 1},
	      { id: 4, date: '6/6/2020, 4:03 AM', user_id: 3},
	      { id: 5, date: '6/6/2020, 5:03 AM', user_id: 7},
	      { id: 6, date: '6/6/2020, 6:03 AM', user_id: 4},
	      { id: 7, date: '6/7/2020, 7:03 AM', user_id: 3},
	      { id: 8, date: '6/7/2020, 8:03 AM', user_id: 6},
	      { id: 9, date: '6/7/2020, 9:03 AM', user_id: 5},
	      { id: 10, date: '6/8/2020, 10:03 AM', user_id: 8},
	      { id: 11, date: '6/8/2020, 11:03 AM', user_id: 9},
	      { id: 12, date: '6/8/2020, 1:03 AM', user_id: 10},
	      { id: 13, date: '6/9/2020, 2:03 AM', user_id: 3},
	      { id: 14, date: '6/9/2020, 3:03 AM', user_id: 2},
	      { id: 15, date: '6/9/2020, 4:03 AM', user_id: 6},
	      { id: 16, date: '7/5/2020, 5:03 AM', user_id: 7},
	      { id: 17, date: '7/5/2020, 6:03 AM', user_id: 2},
	      { id: 18, date: '7/5/2020, 7:03 AM', user_id: 5},
	      { id: 19, date: '7/6/2020, 8:03 AM', user_id: 9},
	      { id: 20, date: '7/6/2020, 9:03 AM', user_id: 10},
	      { id: 21, date: '7/6/2020, 10:03 AM', user_id: 4},
	      { id: 22, date: '7/7/2020, 11:03 AM', user_id: 8},
	      { id: 23, date: '7/7/2020, 1:03 AM', user_id: 9},
	      { id: 24, date: '7/7/2020, 2:03 AM', user_id: 2},
	      { id: 25, date: '7/8/2020, 3:03 AM', user_id: 8},
	      { id: 26, date: '7/8/2020, 4:03 AM', user_id: 7},
	      { id: 27, date: '7/8/2020, 5:03 AM', user_id: 10},
	    ];  
	    return {orders};
	}
}

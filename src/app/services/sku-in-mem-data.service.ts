import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { SKU } from '../classes/sku';

@Injectable({
  providedIn: 'root'
})
export class SKUInMemDataService implements InMemoryDbService {

  	createDb() {
	    let skus: SKU[] = [
	      { id: 1, name: 'Product A', price: 100.00},
	      { id: 2, name: 'Product B', price: 201.00},
	      { id: 3, name: 'Product C', price: 310.00},
	      { id: 4, name: 'Product D', price: 400.00},
	      { id: 5, name: 'Product E', price: 500.00},
	      { id: 6, name: 'Product F', price: 600.00},
	      { id: 7, name: 'Product G', price: 700.00},
	      { id: 8, name: 'Product H', price: 800.00},
	      { id: 9, name: 'Product I', price: 900.00},
	      { id: 10, name: 'Product J', price: 1100.00},
	      { id: 11, name: 'Product K', price: 1200.00},
	      { id: 12, name: 'Product L', price: 1300.00},
	      { id: 13, name: 'Product M', price: 1400.00},
	      { id: 14, name: 'Product N', price: 1500.00},
	      { id: 15, name: 'Product O', price: 1600.00},
	    ];  
	    return {skus};
	}
}

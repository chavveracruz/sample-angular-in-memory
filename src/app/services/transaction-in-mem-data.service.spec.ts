import { TestBed } from '@angular/core/testing';

import { TransactionInMemDataService } from './transaction-in-mem-data.service';

describe('TransactionInMemDataService', () => {
  let service: TransactionInMemDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransactionInMemDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

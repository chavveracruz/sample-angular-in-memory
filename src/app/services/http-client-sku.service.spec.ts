import { TestBed } from '@angular/core/testing';

import { HttpClientSkuService } from './http-client-sku.service';

describe('HttpClientSkuService', () => {
  let service: HttpClientSkuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpClientSkuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

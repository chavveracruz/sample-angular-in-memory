import { TestBed } from '@angular/core/testing';

import { HttpClientTransactionService } from './http-client-transaction.service';

describe('HttpClientTransactionService', () => {
  let service: HttpClientTransactionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpClientTransactionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
 
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SKU } from '../classes/sku';
import { SKUService } from './sku.service';
 
const cudOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'})};
 
@Injectable()
 
export class HttpClientSKUService extends SKUService {
   
  constructor(private http: HttpClient) {
    super();
   }
 
  getSKUs(): Observable<SKU[]> {
    return this.http.get<SKU[]>(this.skusUrl).pipe(
      catchError(this.handleError)
    );
  }
 
  // get by id - will 404 when id not found
  getSKU(id: number): Observable<SKU> {
    const url = `${this.skusUrl}/${id}`;
    return this.http.get<SKU>(url).pipe(
      catchError(this.handleError)
    );
  }
 
  addSKU(name: string, location: string, gender: string, birthdate: string): Observable<SKU> {
    const SKU = { name, location, gender, birthdate };
 
    return this.http.post<SKU>(this.skusUrl, sku, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  deleteSKU(sku: number | SKU): Observable<SKU> {
    const id = typeof SKU === 'number' ? sku : sku.id;
    const url = `${this.skusUrl}/${id}`;
 
    return this.http.delete<SKU>(url, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  searchSKU(term: string): Observable<SKU[]> {
    term = term.trim();
    // add safe, encoded search parameter if term is present
    const options = term ?
    { params: new HttpParams().set('name', term)} : {};
 
    return this.http.get<SKU[]>(this.skusUrl, options).pipe(
      catchError(this.handleError)
    );
  }
 
  updateSKU(sku: SKU): Observable<SKU> {
    return this.http.put<SKU>(this.skusUrl, sku, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
   
  private handleError(error: any) {
    console.error(error);
    return throwError(error);    
  }
 
}
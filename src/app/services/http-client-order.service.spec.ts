import { TestBed } from '@angular/core/testing';

import { HttpClientOrderService } from './http-client-order.service';

describe('HttpClientOrderService', () => {
  let service: HttpClientOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpClientOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

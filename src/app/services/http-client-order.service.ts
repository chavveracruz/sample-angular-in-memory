import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
 
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Order } from '../classes/order';
import { OrderService } from './order.service';
 
const cudOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'})};
 
@Injectable()
 
export class HttpClientOrderService extends OrderService {
   
  constructor(private http: HttpClient) {
    super();
   }
 
  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.ordersUrl).pipe(
      catchError(this.handleError)
    );
  }
 
  // get by id - will 404 when id not found
  getOrder(id: number): Observable<Order> {
    const url = `${this.ordersUrl}/${id}`;
    return this.http.get<Order>(url).pipe(
      catchError(this.handleError)
    );
  }
 
  addOrder(date: string, user_id: number): Observable<Order> {
    const Order = { date, user_id };
 
    return this.http.post<Order>(this.ordersUrl, order, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  deleteOrder(order: number | Order): Observable<Order> {
    const id = typeof Order === 'number' ? order : order.id;
    const url = `${this.ordersUrl}/${id}`;
 
    return this.http.delete<Order>(url, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  updateOrder(order: Order): Observable<Order> {
    return this.http.put<Order>(this.ordersUrl, order, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
   
  private handleError(error: any) {
    console.error(error);
    return throwError(error);    
  }
 
}
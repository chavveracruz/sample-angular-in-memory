import { Order } from '../classes/order';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

	abstract getOrders(): Observable<Order[]>;
	abstract getOrder(id: number): Observable<Order>;
	abstract addOrder(date: string, user_id: number): Observable<Order>;
	abstract deleteOrder(Order: Order | number): Observable<Order>;
	abstract updateOrder(Order: Order): Observable<Order>;
}

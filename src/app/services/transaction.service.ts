import { Transaction } from '../classes/transaction';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

	abstract getTransactions(): Observable<Transaction[]>;
	abstract getTransaction(id: number): Observable<Transaction>;
	abstract addTransaction(sku_id: number, order_id: number): Observable<Transaction>;
	abstract deleteTransaction(Transaction: Transaction | number): Observable<Transaction>;
	abstract updateTransaction(Transaction: Transaction): Observable<Transaction>;
}

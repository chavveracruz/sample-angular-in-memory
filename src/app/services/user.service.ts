import { User } from '../classes/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

	abstract getUsers(): Observable<User[]>;
	abstract getUser(id: number): Observable<User>;
	abstract addUser(name: string, location: string, gender: string, birthdate: string): Observable<User>;
	abstract deleteUser(User: User | number): Observable<User>;
	abstract searchUser(term: string): Observable<User[]>;
	abstract updateUser(User: User): Observable<User>;
}

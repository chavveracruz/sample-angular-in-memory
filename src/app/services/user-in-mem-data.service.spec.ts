import { TestBed } from '@angular/core/testing';

import { UserInMemDataService } from './user-in-mem-data.service';

describe('UserInMemDataService', () => {
  let service: UserInMemDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserInMemDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

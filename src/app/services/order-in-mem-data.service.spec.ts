import { TestBed } from '@angular/core/testing';

import { OrderInMemDataService } from './order-in-mem-data.service';

describe('OrderInMemDataService', () => {
  let service: OrderInMemDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrderInMemDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

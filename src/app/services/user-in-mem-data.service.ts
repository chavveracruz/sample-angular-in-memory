import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from '../classes/user';

@Injectable({
  providedIn: 'root'
})
export class UserInMemDataService implements InMemoryDbService {

	createDb() {
	    let users: User[] = [
	      { id: 1, name: 'Ramon Ramos', location: 'Manila', gender: 'M', birthdate: '09/03/1997'},
	      { id: 2, name: 'Thea Cruz', location: 'Taguig', gender: 'F', birthdate: '03/03/1995'},
	      { id: 3, name: 'Boy Aboonda', location: 'Quezon City', gender: 'M', birthdate: '10/30/1975'},
	      { id: 4, name: 'Chris Aquino', location: 'Quezon City', gender: 'F', birthdate: '01/02/1982'},
	      { id: 5, name: 'Jeyms Yup', location: 'Pasig', gender: 'M', birthdate: '05/15/1988'},
	      { id: 6, name: 'Yvana Onana', location: 'Manila', gender: 'F', birthdate: '02/14/1997'},
	      { id: 7, name: 'Deejay Lunyo', location: 'Marikina', gender: 'M', birthdate: '08/07/1986'},
	      { id: 8, name: 'Lyza Sober', location: 'Bulacan', gender: 'F', birthdate: '06/23/1995'},
	      { id: 9, name: 'Mark Lopez', location: 'San Juan', gender: 'M', birthdate: '04/24/1974'},
	      { id: 10, name: 'Anya Cutis', location: 'Mandaluyong', gender: 'F', birthdate: '06/06/1986'},
	    ];  
	    return {users};
	}
}
